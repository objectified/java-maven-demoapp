package nl.ns.security.demoapp.controllers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class TicketController {
    private final String API_KEY = "verysikret";
    private final String SOME_HARMLESS_SECRET = "donotdetectme"; // pragma: allowlist secret

    @RequestMapping("/")
    public Map<String, String> index() {
        Map map = new HashMap<String, String>();
        map.put("foo", "bar");
        return map;
    }

    @RequestMapping("/vulnerable")
    public String vulnerable(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        return "Intentional CORS misconfiguration";
    }
}
