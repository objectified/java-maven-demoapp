#!/bin/bash

cfn_nag_scan --input-path cloudformation.yml --output-format json > cfn-scan.json
cat cfn-scan.json | python3 json2table.py > cfn-scan.html
