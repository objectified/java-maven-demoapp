pipeline {
    agent any

    tools {
        maven 'Default Maven'
    }

    stages {
        stage('Build') {
            steps {
                git url: 'https://bitbucket.org/objectified/java-maven-demoapp'

                //sh 'mvn clean verify spotbugs:spotbugs sonar:sonar -Dsonar.host.url=http://sonarqube:9000'
                sh 'mvn clean verify spotbugs:spotbugs'
                sh 'mvn sonar:sonar -Dsonar.host.url=http://sonarqube:9000'
            }
        }

	stage('Detect Secrets') {

	    steps {
                script {
                    sh './run-detect-secrets.sh'
                }
            }
	}

	stage('Security Check: Cloudformation template') {
            steps {
                script {
                    sh './run-cfn-scan.sh'
                }
            }
	}

        stage('Security Check: Maven Dependencies') {
            steps {
                dependencyCheck additionalArguments: '', odcInstallation: 'My Dependency-Check'
            }
        }

        stage('Security Check: Dockerfile') {
            steps {
                sh '/hadolint --format checkstyle Dockerfile 2>&1 | tee hadolint-checkstyle-result.xml'
            }
        }

        stage('Publish Docker Image') {
            steps {
                script {
                    docker.build "demoapp"
                }
            }
        }

        stage('Security Check: Scan Docker Image') {
            steps {
                script {
                    //sh "trivy -f table -o trivy-report.txt --exit-code 0 demoapp"
                    sh './run-trivy.sh'
                }
            }
        }


        stage('Start DAST job') {
            steps {
                build job: 'demoapp-dast', wait: false
            }
        }
    }

    post {
        always {
            dependencyCheckPublisher pattern: '**/dependency-check-report.xml'
            archiveArtifacts artifacts: '**/trivy-report.html'
            archiveArtifacts artifacts: '**/secrets.html'
            archiveArtifacts artifacts: '**/cfn-scan.html'
            recordIssues tool: spotBugs(name: 'Code Security')
            recordIssues tool: checkStyle(pattern: '**/hadolint-checkstyle-result.xml', name: 'Dockerfile Security')
	    publishHTML (target: [
	    	allowMissing: false,
		alwaysLinkToLastBuild: false,
		keepAll: true,
		reportDir: '',
		reportFiles: 'trivy-report.html',
		reportName: "Docker Container Scan"
	    ])

	    publishHTML (target: [
	    	allowMissing: false,
		alwaysLinkToLastBuild: false,
		keepAll: true,
		reportDir: '',
		reportFiles: 'secrets.html',
		reportName: "Secrets Scan"
	    ])

	    publishHTML (target: [
	    	allowMissing: false,
		alwaysLinkToLastBuild: false,
		keepAll: true,
		reportDir: '',
		reportFiles: 'cfn-scan.html',
		reportName: "Cloudformation Scan"
	    ])
        }
    }

}
