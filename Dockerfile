FROM openjdk:8-jdk-alpine

COPY target/demoapp-0.0.1-SNAPSHOT.jar demoapp.jar

ENTRYPOINT ["java", "-jar", "/demoapp.jar"]

USER root
