#!/bin/bash

detect-secrets scan > secrets.out
cat secrets.out | python3 json2table.py > secrets.html
